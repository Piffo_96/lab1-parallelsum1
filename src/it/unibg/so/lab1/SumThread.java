package it.unibg.so.lab1;

public class SumThread extends Thread {
	
	private Integer[] values;
	int result;

	public SumThread(Integer[] subList) {
		this.values = subList;
	}
	
	private int sum(){
		int sum = 0;
		for(int i=0; i<values.length; i++)
			sum += values[i];
		return sum;
	}

	@Override
	public void run() {
		result = sum();
	}	

}
